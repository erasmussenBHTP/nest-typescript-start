"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nest_js_1 = require("nest.js");
const app_module_1 = require("./modules/app.module");
const express = require("express");
const bodyParser = require("body-parser");
const instance = express();
instance.use(bodyParser.json());
const app = nest_js_1.NestFactory.create(app_module_1.ApplicationModule, instance);
app.listen(process.env.PORT || 3000, () => console.log('Application is listening on port' + process.env.PORT));
console.log('test value is ' + process.env.TEST);
//# sourceMappingURL=server.js.map
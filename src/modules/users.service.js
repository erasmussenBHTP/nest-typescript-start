"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const nest_js_1 = require("nest.js");
let UsersService = class UsersService {
    constructor() {
        this.users = [
            { id: 1, name: "John Doe" },
            { id: 2, name: "Alice Caeiro" },
            { id: 3, name: "Who Knows" },
        ];
    }
    getAllUsers() {
        return Promise.resolve(this.users);
    }
    getUser(id) {
        const user = this.users.find((user) => user.id == id);
        return Promise.resolve(user);
    }
    addUser(user) {
        this.users.push(user);
        return Promise.resolve();
    }
};
UsersService = __decorate([
    nest_js_1.Component()
], UsersService);
exports.UsersService = UsersService;
//# sourceMappingURL=users.service.js.map
import { Module } from 'nest.js';
import { UsersController } from './users.controller';
import { UsersService } from './users.service';

@Module({
    modules: [],
    controllers: [UsersController],
    components: [UsersService]
})
export class ApplicationModule {}
import { Controller, Get, Post, HttpStatus, Request, Response, Param, Body } from 'nest.js';
import * as express from 'express';
import * as pg from 'pg';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
    constructor(private usersService: UsersService) { }

    @Get()
    public async getAllUsers(@Request() req: express.Request, @Response() res: express.Response, next: express.NextFunction) {
        const users = await this.usersService.getAllUsers();
        res.status(HttpStatus.OK).json(users);
    }

    @Get('/:id')
    public async getUser(@Response() res, @Param('id') id: number) {
        pg.defaults.ssl = true;
        console.log('connecting to db');
        pg.connect(process.env.DATABASE_URL, (err: Error, client: pg.Client, done) => {
            if (err){
                console.log(err);
            }

            console.log('sending query');
            client.query('UPDATE salesforce.Contact SET FirstName = $1 WHERE sfid = $2',
            [id, '0034600000HxPYnAAN'],
            (err: Error, result: pg.QueryResult) => {
                if (err) {
                    console.log(err);
                }
                else if (result){
                    console.log('rows ' + result.rowCount);
                }

                done();
            })
        });

        const user = await this.usersService.getUser(id)
        if (user) {
            res.status(HttpStatus.OK).json(user)
        }
        else {
            res.status(HttpStatus.NOT_FOUND).json({});
        }
    }

    @Post()
    public async addUser(@Response() res, @Body('user') user) {
        await this.usersService.addUser(user);
        res.status(HttpStatus.CREATED).json({ message: 'Created' });
    }
}
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const nest_js_1 = require("nest.js");
const express = require("express");
const pg = require("pg");
const users_service_1 = require("./users.service");
let UsersController = class UsersController {
    constructor(usersService) {
        this.usersService = usersService;
    }
    getAllUsers(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            const users = yield this.usersService.getAllUsers();
            res.status(nest_js_1.HttpStatus.OK).json(users);
        });
    }
    getUser(res, id) {
        return __awaiter(this, void 0, void 0, function* () {
            pg.defaults.ssl = true;
            console.log('connecting to db');
            pg.connect(process.env.DATABASE_URL, (err, client, done) => {
                if (err) {
                    console.log(err);
                }
                console.log('sending query');
                client.query('UPDATE salesforce.Contact SET FirstName = $1 WHERE sfid = $2', [id, '0034600000HxPYnAAN'], (err, result) => {
                    if (err) {
                        console.log(err);
                    }
                    else if (result) {
                        console.log('rows ' + result.rowCount);
                    }
                    done();
                });
            });
            const user = yield this.usersService.getUser(id);
            if (user) {
                res.status(nest_js_1.HttpStatus.OK).json(user);
            }
            else {
                res.status(nest_js_1.HttpStatus.NOT_FOUND).json({});
            }
        });
    }
    addUser(res, user) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.usersService.addUser(user);
            res.status(nest_js_1.HttpStatus.CREATED).json({ message: 'Created' });
        });
    }
};
__decorate([
    nest_js_1.Get(),
    __param(0, nest_js_1.Request()), __param(1, nest_js_1.Response()),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object, Function]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getAllUsers", null);
__decorate([
    nest_js_1.Get('/:id'),
    __param(0, nest_js_1.Response()), __param(1, nest_js_1.Param('id')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Number]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "getUser", null);
__decorate([
    nest_js_1.Post(),
    __param(0, nest_js_1.Response()), __param(1, nest_js_1.Body('user')),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", Promise)
], UsersController.prototype, "addUser", null);
UsersController = __decorate([
    nest_js_1.Controller('users'),
    __metadata("design:paramtypes", [users_service_1.UsersService])
], UsersController);
exports.UsersController = UsersController;
//# sourceMappingURL=users.controller.js.map
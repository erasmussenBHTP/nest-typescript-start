import { NestFactory } from 'nest.js';
import { ApplicationModule } from './modules/app.module';
import * as express from 'express';
import * as bodyParser from 'body-parser';

const instance = express();
instance.use(bodyParser.json());


const app = NestFactory.create(ApplicationModule, instance);
app.listen(process.env.PORT || 3000, () => console.log('Application is listening on port' + process.env.PORT));
console.log('test value is ' + process.env.TEST);